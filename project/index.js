const express = require("express");
const moment = require("moment");
const fetch = require("node-fetch");
const _ = require("lodash");
const router = express.Router();
const printToFile = require("./botinfotocsv/helper/exportTocsv");
const app = express();
const path = require("path");

//global declarations
let accessToken, agent_id, base_url, startEpoch, endEpoch;
let urls = [];
let requestOptions = {};
let user_Arr = [];
let finalArr = [];
let count = 0,
  totalPages;
const per_page = 1000;
let parallel_count = 6;

//app.use("/data3.csv", express.static(path.join(__dirname, "/data3.csv")));
app.get("/", async function (req, res) {
  const startDate = req.query.start_time;
  console.log(startDate);
  startEpoch = moment(startDate, "DD/MM/YYYY").unix();
  //coverting from DD/mm/yyyy format to epoch timing
  console.log(startEpoch);

  const endDate = req.query.end_time;
  console.log(endDate);
  endEpoch = moment(endDate, "DD/MM/YYYY").unix();
  //converting from DD/MM/YYYY format to epoch timing
  console.log(endEpoch);
  base_url = req.query.base_url;
  accessToken = req.rawHeaders[1];
  agent_id = req.query.agent_id;
  console.log(base_url);
  console.log(agent_id);
  console.log(accessToken);

  await filterData(1);
  //await res.json(finalArr);
  let filestatus = await printToFile(finalArr);

  if (filestatus) {
    console.log("filestatus,,true");

    let options = {
      root: path.join(__dirname),
    };
    let fileName = "/data3.csv";
    res.sendFile(fileName, options, function (err) {
      if (err) {
        next(err);
      } else {
        console.log("Sent:", fileName);
      }
    });
  } else {
    res.statusCode = 500;
    res.setHeader("Content-Type", "application/json");
    res.send({ message: "Error in sending file." });
  }
});

const filterData = async (page_no) => {
  const myHeaders = {
    "Access-Token": `${accessToken}`,
  };
  console.log(myHeaders);
  requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };
  await fetch(
    `${base_url}/api/v1/agents/${agent_id}/intents/qna.json?start_time=${startEpoch}&end_time=${endEpoch}&per_page=200&page_no=${page_no}`,
    requestOptions
  )
    .then((response) => response.text())
    .then((result) => {
      totalPages = JSON.parse(result).total_pages;
      console.log("totalPages", totalPages);
      for (let i = count + 1; i <= count + parallel_count; i++) {
        if (i <= totalPages) {
          urls.push(
            fetch(
              `${base_url}/api/v1/agents/${agent_id}/intents/qna.json?start_time=${startEpoch}&end_time=${endEpoch}&per_page=200&page_no=${i}`,
              requestOptions
            ).then((response) => {
              if (response.status == 200) {
                response.json().then((dataobtained) => {
                  processJSON(dataobtained);
                  finalPush(user_Arr);
                });
              }
            })
          );
        }
      }
    })
    .catch((error) => {
      console.log(error);
    });
  await Promise.all(urls);
  urls = [];
  if (count <= totalPages) {
    count += parallel_count;
    await filterData(1);
  } else {
    //console.log(user_Arr);
    return await finalArr;
  }
};

const processJSON = (dataobtained) => {
  let questions = [];
  dataobtained.intents
    ? dataobtained.intents.forEach((item) => questions.push(item.name))
    : null;
  questions = _.uniq(questions);
  questions.forEach((name) => {
    let training = [],
      skills = [];

    dataobtained.intents.forEach((sk) => {
      if (name == sk.name) {
        skills.push(sk.skill.name);
      }
    });
    dataobtained.intents.forEach((sk) => {
      if (name == sk.name) {
        sk.training_data.map((td) => {
          training.push(td?.value);
        });
      }
    });
    user_Arr.push({
      Questions: name,
      skillName: skills.reverse(),
      trainingNames: training.reverse(),
    });
  });
  return user_Arr;
};

const finalPush = async function (user_Arr) {
  let questionName, skillName, trainingData;
  user_Arr.forEach((first) => {
    (questionName = first.Questions),
      (skillName = first.skillName[0]),
      first.trainingNames.forEach((second) => {
        trainingData = second;
        finalArr.push({ skillName, questionName, trainingData });
        //console.log(finalArr);
      });
  });
};

// function printToFile(inp) {
//   fastcsv
//     .write(inp, { headers: true })
//     .on("finish", function () {
//       console.log("Write to CSV successfully!");
//     })
//     .pipe(ws);
// }

app.listen(3000, () => {
  console.log("Started on PORT 3000");
});
