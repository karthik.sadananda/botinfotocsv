const fastcsv = require("fast-csv");
const fs = require("fs");
const ws = fs.createWriteStream("data3.csv");
module.exports = async (inp) => {
  return fastcsv
    .write(inp, { headers: true })
    .on("finish", function () {
      console.log("Write to CSV successfully!");
    })
    .pipe(ws)
    .on("end", () => {
      return true;
    });
};
